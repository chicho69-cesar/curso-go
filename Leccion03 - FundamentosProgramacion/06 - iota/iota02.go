package main

import (
	"fmt"
)

const (
	c0 = iota // Inicia en 0
	c1 // 1
	c2 // 2
)

func main() {
	fmt.Println(c0)
	fmt.Println(c1)
	fmt.Println(c2)
}
