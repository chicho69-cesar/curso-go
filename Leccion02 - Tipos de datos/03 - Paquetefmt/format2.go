package main

import (
	"fmt"
)

var a int
var b string = "James Bond"

func main() {
	fmt.Printf("%v\n", a) // Usamos el verbo variable
	fmt.Printf("%v", b)
}
