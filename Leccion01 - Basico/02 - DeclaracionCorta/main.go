package main

import (
	"fmt"
)

func main() {
	x := 42 + 1 // Declaracion corta
	y := "Cesar Villalobos Olmos"
	fmt.Println(x)
	fmt.Println(y)

	x = 50
	fmt.Println(x)
}
