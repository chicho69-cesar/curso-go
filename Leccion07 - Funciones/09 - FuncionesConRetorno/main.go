/* 
Puedes retornar una función desde una función.
*/

package main

import (
	"fmt"
)

func main() {
	s1 := foo() // asignamos el valor de retorno
	fmt.Println(s1)

	x := bar() // funcion que regresa una funcion

	fmt.Printf("%T\n", x)

	i := x() // funcion que fue devuelta
	fmt.Println(i)

	fmt.Println(bar()()) // llamamos directamente a la funcion que nos es retornada
}

func foo() string {
	s := "Hola mundo."
	return s
}

// clousure
func bar() func() int {
	return func() int {
		return 123
	}
}
