/*
Asignando una func a una variable
*/

package main

import "fmt"

func main() {
	// le podemos asignar una funcion a una variable
	f := func() {
		fmt.Println("Expresión función")
	}
	f() // ejecutamos la funcion expresión

	// funcion expresion que recibe un parametro
	g := func(x int) {
		fmt.Println("El año del descubrimiento de América fue:", x)
	}
	g(1492)

	// funcion expresion que regresa un valor
	r := func (x int) int {
		return x * 2
	}
	fmt.Println("El doble de 15 es = ", r(15))
}
