# Tipo de canal. Lee de izquierda a derecha.

***código:***
- Viendo el tipo
    - Código del video anterior
        - https://play.golang.org/p/a98otBr4eX
    - Enviando y recibiendo (bidireccional)
        - https://play.golang.org/p/TD7DXPWTrFw
        - “Enviar y recibir” significa “enviar y recibir”
            - https://play.golang.org/p/SHr3lpX4so
                - Ya visto en el código arriba
        - Enviar significa enviar
            - error: “operación inválida: <-cs (recibir desde un canal send-only chan<-int)”
                - https://play.golang.org/p/oB-p3KMiH6
        - Recibir significa recibir
            - error: “operación inválida: cr <- 42 (enviar a un canal receive-only tipo<-chan int)”
                - https://play.golang.org/p/_DBRueImEq