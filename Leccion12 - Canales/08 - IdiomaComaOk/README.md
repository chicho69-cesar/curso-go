# Idioma Coma ok

El idioma coma ok con select.
código:
- Cerrando el canal salir y el idioma coma ok
    - https://play.golang.org/p/2fgKUziopJz
    - con bool
        * https://play.golang.org/p/mcQksC7pHiQ
    - Con int
        * https://play.golang.org/p/sK1f_oGfeSu
- Sólo el idioma coma ok
    - https://play.golang.org/p/6LPzCtZeT3
    - https://play.golang.org/p/dToDc0zJhZ
- Limpieza del código arriba - idioma coma ok
    - paso 1 - código del idioma coma ok reducido
        * https://play.golang.org/p/-sTOZRwFaa9
    - step 2 - eliminar los underscores - desecho de variable
        * https://play.golang.org/p/1dwId-jwauQ
    - step 3 - cambiar salir de bool a int
        * https://play.golang.org/p/0a62VkopvVf
- Select para recibir
    - https://play.golang.org/p/phXtIWpbrUe
- Select para enviar
    - https://play.golang.org/p/SKMuLIHTKyN
- Interesante
    - No corre
        * https://play.golang.org/p/sBJV_HAFIA_O
    - Corre
        * https://play.golang.org/p/MvVhDtZs7cs
    - Corre de otra forma - Este me gusta más
        * El canal salir fue eliminado
        * Select fue eliminado
          - Haz range sobre el canal usado
        * https://play.golang.org/p/b9yImbB7lGt