# Paquetes

- Una carpeta, varios archivos
    - Declaración de paquete en cada archivo.
    - Scope de paquete
        - Las cosas de un archivo pueden ser accesibles desde otro archivo
    - Los imports tienen scope de archivo
- Exportado / No exportado
    - También, visible / no visible
    - No decimos (generalmente hablando): público / privado
    - Usar mayúsculas
        - Escribir con mayúsculas: exportado, visible fuera del paquete
        - Minúsculas: no exportado, no visible fuera del paquete.
