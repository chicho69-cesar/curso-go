# Concurrencia vs paralelismo

Aunque cuando la gente escucha la palabra `concurrencia` frecuentemente piensan en
`paralelismo`, un concepto relacionado pero que realmente son diferentes. En
programación, `concurrencia` es la composición de independientemente ejecutar
procesos, mientras que `paralelismo` es la ejecución simultánea de (posiblemente
relacionados) procesos computacionales. La `concurrencia` se trata de lidiar con muchas
cosas a la vez. El `paralelismo` se trata de hacer muchas cosas a la vez.