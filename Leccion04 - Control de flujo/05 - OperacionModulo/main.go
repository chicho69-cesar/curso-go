package main

import (
	"fmt"
)

func main() {
	x := 83 / 40 // Division no exacta
	y := 83 % 40 // Obtenemos el residuo de la division

	fmt.Println(x, y)
}
