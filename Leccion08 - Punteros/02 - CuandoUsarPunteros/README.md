## Cuando usar pointers

Los pointers te permiten compartir un valor almacenado en alguna ubicación de la memoria.

Usa pointers cuando: 
1. No quieres pasar una cantidad grande de datos
2. Quieres cambiar los datos en esa ubicación

***Todo en Go es pasado por valor***. Olvida cualquier concepto o frase puedas traer de otros
lenguajes. Pasar por referencia, pasar por copia - olvida esas frases. “Pasar por valor.” Ésa es
la única frase que deberías saber y recordar. Es la única frase que deberías usar. Pasar por
valor. Todo en Go es pasado por valor. En Go, lo que ves es lo que obtienes - what you see is
what you get (wysiwyg). Mira lo que está ocurriendo. Éso es lo que obtienes